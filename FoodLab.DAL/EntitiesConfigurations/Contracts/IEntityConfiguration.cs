﻿using FoodLab.DAL.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;


namespace FoodLab.DAL.EntitiesConfigurations.Contracts
{
    public interface IEntityConfiguration
    {
        public interface IEntityConfiguration<T> where T : class, IEntity
        {
            Action<EntityTypeBuilder<T>> ProvideConfigurationAction();
        }
    }
}
