﻿using FoodLab.DAL.Entities.Contracts;
using FoodLab.DAL.Repositories.Contracts;
using System;
using System.Collections.Generic;
using System.Text;

namespace FoodLab.DAL.Repositories
{
    public class UnitOfWorkFactory : IUnitOfWorkFactory
    {
        private readonly IApplicationDbContextFactory _applicationDbContextFactory;

        public UnitOfWorkFactory(IApplicationDbContextFactory applicationDbContextFactory)
        {
            _applicationDbContextFactory = applicationDbContextFactory;
        }

        public UnitOfWork Create()
        {
            return new UnitOfWork(_applicationDbContextFactory.Create());
        }
    }
}
