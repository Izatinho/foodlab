﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FoodLab.DAL.Repositories.Contracts
{
    public interface IUnitOfWorkFactory
    {
        UnitOfWork Create();
    }
}
