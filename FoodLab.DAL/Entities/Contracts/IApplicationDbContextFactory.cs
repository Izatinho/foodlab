﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FoodLab.DAL.Entities.Contracts
{
    public interface IApplicationDbContextFactory
    {
        ApplicationDbContext Create();
    }
}
