﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FoodLab.DAL.Entities
{
    public interface IEntity
    {
        int Id { get; set; }
    }
}
