﻿using Microsoft.AspNetCore.Identity;

namespace FoodLab.DAL.Entities
{
    public class User : IdentityUser<int>, IEntity
    {
        public int Id { get; set; } 
        public string Bucket { get; set; }  
    }
}
